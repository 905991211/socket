package pin;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.OptionalInt;

public class Controller {
    private Context context;

    public Controller(Context context) {
        this.context = context;
    }

    public void route(HttpMess httpMess){
        try {
            switch (httpMess.path){
                case "/add":
                    add(httpMess);
                    break;
                case "/mult":
                    mult(httpMess);
                    break;
                default:
                    context.writeAndClose("There is no matching path");
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }

    }
    public void add(HttpMess httpMess) throws IOException {
        Map<String, String> param = httpMess.param;
        int sum = param.values()
                .stream()
                .mapToInt(Integer::valueOf)
                .sum();
        String result = "sum : " + sum;
        context.writeAndClose(result);
    }
    public void mult(HttpMess httpMess) throws IOException {
        Map<String, String> param = httpMess.param;
        int mult = param.values()
                .stream()
                .mapToInt(Integer::valueOf)
                .reduce((a, b) -> a * b)
                .orElse(0);

        String result = "mult : " + mult;
        context.writeAndClose(result);

    }
}
