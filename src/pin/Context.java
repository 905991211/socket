package pin;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class Context {
    private final SocketChannel channel;
    private final NioLoop nioLoop;
    private HttpMess httpMess;
    public Context(SocketChannel channel, NioLoop nioLoop) {
        this.channel = channel;
        this.nioLoop = nioLoop;
        Controller controller = new Controller(this);
        this.httpMess=new HttpMess(this, controller);
    }



    public void writeAndClose(String s) throws IOException {
        if(channel.isOpen()) {
            channel.write(ByteBuffer.wrap(s.getBytes(StandardCharsets.UTF_8)));
            channel.close();
            nioLoop.getMap().remove(channel);
        }
    }
    public void fireRead(ByteBuffer byteBuffer){
        this.httpMess.read(this,byteBuffer);
    }


    public SocketChannel getChannel() {
        return channel;
    }

}
