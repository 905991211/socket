package pin;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class HttpMess {
    public Map<String,String> param;
    public String path;
    private StringBuilder sb =new StringBuilder();
    private String msg;
    private final Controller controller;
    private final Context context;
    private final CharsetDecoder charsetDecoder;

    public HttpMess(Context context, Controller controller) {
        this.controller = controller;
        this.context = context;
        this.charsetDecoder = StandardCharsets.UTF_8.newDecoder();
    }

    public void read(Context ctx, ByteBuffer buffer){

        CharBuffer charBuffer =null;
        try {
            charBuffer=charsetDecoder.decode(buffer);
        } catch (CharacterCodingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        String s = charBuffer.toString();

        sb.append(s);
        if((s.length()>=4 && s.endsWith("\r\n\r\n"))){
            parse(false);
        }
        msg = sb.toString();
        if(msg.length()>=4 && msg.endsWith("\r\n\r\n")){
            parse(true);
        }

    }
    public void parse(boolean isString){
        if(!isString)
            msg=sb.toString();
        System.out.println();
        System.out.println("-------------------http-start----------------------");
        System.out.println(msg);
        System.out.println("-------------------http-end------------------------");
        System.out.println();

        // 按行分割
        String[] split = msg.split("\r\n");
        if(split.length>0){
            // 处理第一行
            String fline = split[0];
            String[] s = fline.split(" ");
            String pathPart=s[1];
            int j = pathPart.indexOf('?');
            if(j==-1 || j==pathPart.length()) {
                this.param=new HashMap<>(0);
                this.path= pathPart;
                this.controller.route(this);
                return;
            }
            this.path=this.path==null?pathPart.substring(0,j):path;
            String paramString = pathPart.substring(j + 1);
            String[] params = paramString.split("&");
            this.param=new HashMap<>(4);
            for (String p : params){
                String[] kv = p.split("=");
                this.param.put(kv[0],kv[1]);
            }
            controller.route(this);
        }

    }

}
